# tomcat-launcher

Launch web applications with Tomcat.

# Usage

Launch a war-file on HTTP port 8080:
```java
TomcatLauncher.configure()
        .addConnector(8080)
        .addApplication("", "/path/to/warfile.war")
        .apply().launch();
```
Launch a war-file on HTTPS port 8443:
```java
TomcatLauncher.configure()
        .addSecureConnector(8443, Paths.get("keystore.jks"), "password")
        .addApplication("", "/path/to/warfile.war")
        .apply().launch();
```

