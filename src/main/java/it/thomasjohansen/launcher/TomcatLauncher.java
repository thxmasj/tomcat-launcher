package it.thomasjohansen.launcher;

import org.apache.catalina.Context;
import org.apache.catalina.WebResourceRoot;
import org.apache.catalina.Wrapper;
import org.apache.catalina.connector.Connector;
import org.apache.catalina.core.StandardContext;
import org.apache.catalina.manager.ManagerServlet;
import org.apache.catalina.startup.Tomcat;
import org.apache.catalina.webresources.DirResourceSet;
import org.apache.catalina.webresources.StandardRoot;

import javax.servlet.ServletException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.lang.System.console;

/**
 * Launch web applications (WAR files) with Tomcat.
 */
public class TomcatLauncher {

    private static final String privateKeyStoreResource = "/tls.jks";
    private Path baseDir;
    private List<ConnectorDescriptor> connectorDescriptors = new ArrayList<>();
    private List<ApplicationDescriptor> applicationDescriptors = new ArrayList<>();
    private boolean enableManager;
    private String managerContextPath = "/manager";

    public static void main(String[] args) throws Exception {
        if (args.length > 0)
            configureFromArguments(args);
        else
            configureDefault();
    }

    private static void configureDefault() throws Exception {
        configure()
                .addConnector(8080)
                .addApplication(
                        "",
                        TomcatLauncher.class.getProtectionDomain().getCodeSource().getLocation().getFile()
                )
                .enableManager()
                .apply()
                .launch();
    }

    private static void configureFromArguments(String[] args) throws Exception {
        List<String> arguments = Arrays.asList(args);
        Path baseDir = Files.createTempDirectory("TomcatLauncher");
        Configurer configurer = configure().baseDir(baseDir).enableManager();
        for (String argument : arguments) {
            if (argument.matches("\\d+")) {
                if (isSecured()) {
                    String keyStorePassword = getPrivateKeyPassword();
                    configurer.addSecureConnector(
                            Integer.parseInt(argument),
                            createPrivateKeyStore(
                                    baseDir,
                                    keyStorePassword,
                                    privateKeyStoreResource
                            ),
                            keyStorePassword
                    );
                } else
                    configurer.addConnector(Integer.parseInt(argument));
            } else if (argument.matches("/.*=.*\\.war")) {
                configurer.addApplication(argument.split("=")[0], argument.split("=")[1]);
            } else {
                configurer.addApplication(
                        argument,
                        TomcatLauncher.class.getProtectionDomain().getCodeSource().getLocation().getFile()
                );
            }
        }
        configurer.apply().launch();
    }

    private TomcatLauncher() {
    }

    public static Configurer configure() {
        return new Configurer(new TomcatLauncher());
    }

    public void launch() throws Exception {
        final Tomcat tomcat = new Tomcat();
        tomcat.setBaseDir(baseDir.toAbsolutePath().toString());
        for (ConnectorDescriptor connectorDescriptor : connectorDescriptors) {
            if (connectorDescriptor.getKeyStorePath() != null)
                addSecureConnector(
                        tomcat,
                        connectorDescriptor.getPort(),
                        connectorDescriptor.getKeyStorePath(),
                        connectorDescriptor.getKeyStorePassword()
                );
            else
                addConnector(tomcat, connectorDescriptor.getPort());
        }
        for (ApplicationDescriptor applicationDescriptor : applicationDescriptors) {
            addWebApplication(
                    tomcat,
                    baseDir,
                    applicationDescriptor.getContextPath(),
                    applicationDescriptor.getLocation()
            );
        }
        // Start all applications in parallel
        tomcat.getHost().setStartStopThreads(applicationDescriptors.size());
        if (enableManager) {
            addManagerServlet(tomcat, managerContextPath);
        }
        Runtime.getRuntime().addShutdownHook(new WorkFileRemover(baseDir));
        tomcat.start();
        tomcat.getServer().await();
    }

    private static boolean isSecured() throws IOException {
        return TomcatLauncher.class.getResourceAsStream(privateKeyStoreResource) != null;
    }

    private Context addWebApplication(
            Tomcat tomcat,
            Path baseDir,
            String contextPath,
            String location
    ) throws ServletException, IOException, URISyntaxException {
        StandardContext context = (StandardContext)tomcat.addWebapp(contextPath, location);
        handleRunningFromMavenWorkspace(location, context);
        // Note that class loading is extremely slow with unpackWAR=false, so start-up and first request(s) might take
        // long time (up to minutes).
        context.setUnpackWAR(true);
        // Directory "webapps" is not used when unpackWAR is false
        if (context.getUnpackWAR() && !Files.exists(baseDir.resolve("webapps")))
            Files.createDirectory(baseDir.resolve("webapps"));
        return context;
    }

    private void handleRunningFromMavenWorkspace(
            String location,
            StandardContext context
    ) throws URISyntaxException {
        File locationFile = new File(location);
        if (locationFile.getName().equals("classes") && locationFile.getParentFile().getName().equals("target")) {
            // Handle running from Maven workspace.
            // - "target/classes" must be mounted on WEB-INF/classes.
            // - "src/main/webapp" must be mounted on /.
            WebResourceRoot resourceRoot = new StandardRoot(context);
            resourceRoot.addPreResources(
                    new DirResourceSet(
                            resourceRoot,
                            "/WEB-INF/classes",
                            locationFile.getAbsolutePath(),
                            "/"
                    )
            );
            File workspaceRoot = new File(location, "../../src/main/webapp");
            if (workspaceRoot.exists()) {
                resourceRoot.addPreResources(
                        new DirResourceSet(
                                resourceRoot,
                                "/",
                                new URI(workspaceRoot.getAbsolutePath()).normalize().getPath(),
                                "/"
                        )
                );
            }
            context.setResources(resourceRoot);
        }
    }

    private void addManagerServlet(Tomcat tomcat, String contextPath) {
        Context managerContext = tomcat.addContext(contextPath, "/tmp");
        ManagerServlet managerServlet = new ManagerServlet();
        Wrapper wrapper = Tomcat.addServlet(managerContext, "manager", managerServlet);
        wrapper.setLoadOnStartup(1);
        wrapper.addMapping("/*");
        managerServlet.setWrapper(wrapper);
    }

    private static Path createPrivateKeyStore(Path directory, String password, String resource) throws CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException {
        return createKeyStore(directory, "privateKeyStore", resource, password);
    }

    private static Path createKeyStore(Path directory, String fileName, String resourceName, String password) throws KeyStoreException, CertificateException, NoSuchAlgorithmException, IOException {
        InputStream in = TomcatLauncher.class.getResourceAsStream(resourceName);
        KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
        keyStore.load(in, password.toCharArray());
        Path file = directory.resolve(fileName);
        FileOutputStream out = new FileOutputStream(file.toFile());
        keyStore.store(out, password.toCharArray());
        return file.toAbsolutePath();
    }

    private static String getPrivateKeyPassword() {
        return System.getProperty("javax.net.ssl.keyStorePassword",
                console() != null
                        ? String.valueOf(console().readPassword("Private key password> "))
                        : "changeit");
    }

    private void addSecureConnector(
            Tomcat tomcat,
            int port,
            Path keyStorePath,
            String password
    ) throws CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException {
        Connector connector = new Connector();
        connector.setPort(port);
        connector.setSecure(true);
        connector.setScheme("https");
//        connector.setAttribute("ciphers", "TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256,TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA,TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA384,TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA, TLS_ECDHE_RSA_WITH_RC4_128_SHA,TLS_RSA_WITH_AES_128_CBC_SHA256,TLS_RSA_WITH_AES_128_CBC_SHA, TLS_RSA_WITH_AES_256_CBC_SHA256,TLS_RSA_WITH_AES_256_CBC_SHA,SSL_RSA_WITH_RC4_128_SHA");
        connector.setAttribute("keystoreFile", keyStorePath.toString());
        connector.setAttribute("keystorePass", password);
//        connector.setAttribute("truststoreFile", trustStorePath.toAbsolutePath().toString());
//        connector.setAttribute("truststorePass", "changeit");
//        connector.setAttribute("clientAuth", true);
        connector.setAttribute("SSLEnabled", "true");
        connector.setAttribute("sslEnabledProtocols", "TLSv1.2");
        connector.setAttribute("sslProtocol", "TLSv1.2");
        tomcat.getService().addConnector(connector);
        tomcat.setConnector(connector);
    }

    private void addConnector(Tomcat tomcat, int port) {
        Connector connector = new Connector();
        connector.setPort(port);
        tomcat.getService().addConnector(connector);
        tomcat.setConnector(connector);
    }

    static class WorkFileRemover extends Thread {

        private Path baseDir;

        public WorkFileRemover(Path baseDir) {
            this.baseDir = baseDir;
        }

        @Override
        public void run() {
            try {
                deleteRecursive(baseDir.toFile());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

        public boolean deleteRecursive(File path) throws FileNotFoundException{
            if (!path.exists()) throw new FileNotFoundException(path.getAbsolutePath());
            boolean ret = true;
            if (path.isDirectory()){
                File[] files = path.listFiles();
                if (files != null) {
                    for (File f : files) {
                        ret = ret && deleteRecursive(f);
                    }
                }
            }
            return ret && path.delete();
        }

    }

    static class ConnectorDescriptor {

        private int port;
        private Path keyStorePath;
        private String keyStorePassword;

        public ConnectorDescriptor(int port) {
            this.port = port;
        }

        public ConnectorDescriptor(int port, Path keyStorePath, String keyStorePassword) {
            this.port = port;
            this.keyStorePath = keyStorePath;
            this.keyStorePassword = keyStorePassword;
        }

        public int getPort() {
            return port;
        }

        public Path getKeyStorePath() {
            return keyStorePath;
        }

        public String getKeyStorePassword() {
            return keyStorePassword;
        }

    }

    static class ApplicationDescriptor {

        private String contextPath;
        private String location;

        public ApplicationDescriptor(String contextPath, String location) {
            this.contextPath = contextPath;
            this.location = location;
        }

        public String getContextPath() {
            return contextPath;
        }

        public String getLocation() {
            return location;
        }

    }

    public static class Configurer {

        private TomcatLauncher instance;

        public Configurer(TomcatLauncher instance) {
            this.instance = instance;
        }

        public Configurer addConnector(int port) {
            instance.connectorDescriptors.add(new ConnectorDescriptor(port));
            return this;
        }

        public Configurer addSecureConnector(int port, Path keyStorePath, String password) {
            instance.connectorDescriptors.add(new ConnectorDescriptor(port, keyStorePath, password));
            return this;
        }

        public Configurer addApplication(String contextPath, String location) {
            instance.applicationDescriptors.add(new ApplicationDescriptor(
                    contextPath,
                    location
            ));
            return this;
        }

        public Configurer enableManager() {
            instance.enableManager = true;
            instance.managerContextPath = "/manager";
            return this;
        }

        @SuppressWarnings("unused")
        public Configurer enableManager(String contextPath) {
            instance.enableManager = true;
            instance.managerContextPath = contextPath;
            return this;
        }

        public Configurer baseDir(Path baseDir) {
            instance.baseDir = baseDir;
            return this;
        }

        public TomcatLauncher apply() throws IOException {
            if (instance.baseDir == null)
                instance.baseDir = Files.createTempDirectory("TomcatLauncher");
            return instance;
        }

    }

}
